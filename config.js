module.exports = {
  siteTitle: 'Curriculum Vitae', // <title>
  manifestName: 'Curriculum',
  manifestShortName: 'Landing', // max 12 characters
  manifestStartUrl: '/',
  manifestBackgroundColor: '#663399',
  manifestThemeColor: '#663399',
  manifestDisplay: 'standalone',
  manifestIcon: 'src/assets/img/website-icon.png',
  // pathPrefix: `/gatsby-starter-resume/`, // This path is subpath of your hosting https://domain/portfolio
  firstName: 'Adrian',
  lastName: 'Queipo',
  // social
  socialLinks: [
    {
      icon: 'fa-github',
      name: 'Github',
      url: 'https://github.com/aqp696',
    },
    {
      icon: 'fa-gitlab',
      name: 'Gitlab',
      url: 'https://gitlab.com/aqueipo',
    },
    {
      icon: 'fa-linkedin-in',
      name: 'Linkedin',
      url: 'https://linkedin.com/in/aqueipo/',
    },
    {
      icon: 'fa-twitter',
      name: 'Twitter',
      url: 'https://twitter.com/aqueipo',
    },
    {
      icon: 'fa-facebook-f',
      name: 'Facebook',
      url: 'https://facebook.com/aqueipo',
    },
  ],
  email: 'aqueipo@gmail.com',
  phone: '610377656',
  address: 'Ourense, España',
};
