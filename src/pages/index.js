import React from 'react';

import Layout from '../components/Layout';
import Sidebar from '../components/Sidebar';
import About from '../components/About';
import Education from '../components/Education';
import Skills from '../components/Skills';
import Interests from '../components/Interests';
import Experiences from '../components/Experiences';
import Certifications from '../components/Certifications';

const IndexPage = ({ data }) => (
  <Layout>
    <Sidebar />
    <div className="container-fluid p-0">
      <About />
      <hr className="m-0" />
      <Experiences experiences={data.experiences} />

      <hr className="m-0" />
      <Education />


      <hr className="m-0" />
      <Skills />

      <hr className="m-0" />
      <Interests />

      <hr className="m-0" />
      <Certifications certifications={data.certifications} />

    </div>
  </Layout>
);

export default IndexPage;

export const pageQuery = graphql`
query QueryBlogs {
  certifications: allMarkdownRemark(filter: { frontmatter:  { content: { eq:"certifications"}}}) {
    edges {
      node {
        frontmatter {
          title
          date
          bag_url
        }
        html
      }
    }
  }
  experiences: allMarkdownRemark(filter: {frontmatter: {content: {eq: "experiences"}}}, sort: {fields: frontmatter___start, order: DESC}) {
    edges {
      node {
        frontmatter {
          title
          job
          company
          start
          end
          date
        }
        html
      }
    }
  }
}
`
