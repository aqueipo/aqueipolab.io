import React, { Component } from 'react';

export class Education extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { tabs } = this.state;
    return (
	          <section
        className="resume-section p-3 p-lg-5 d-flex align-items-center"
        id="education"
      >
        <div className="w-100">
          <h2 className="mb-5">Educación</h2>

          <div className="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
            <div className="resume-content">
              <h3 className="mb-0">University of Colorado Boulder</h3>
              <div className="subheading mb-3">Bachelor of Science</div>
              <div>Computer Science - Web Development Track</div>
              <p>GPA: 3.23</p>
            </div>
            <div className="resume-date text-md-right">
              <span className="text-primary">Mayo 2014</span>
            </div>
          </div>

          <div className="resume-item d-flex flex-column flex-md-row justify-content-between">
            <div className="resume-content">
              <h3 className="mb-0">Universidad Laboral</h3>
              <div className="subheading mb-3">Bachillerato Científico-Tecnológico</div>
            </div>
            <div className="resume-date text-md-right">
              <span className="text-primary">Junio 2002</span>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Education;

