import React, { Component } from 'react';
import config from '../../config';

export class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { tabs } = this.state;
    return (
	    <section
        className="resume-section p-3 p-lg-5 d-flex align-items-center"
        id="about"
      >
        <div className="w-100">
          <h1 className="mb-0">
            {config.firstName}
            <span className="text-primary">{config.lastName}</span>
          </h1>
          <div className="subheading mb-5">
            {config.address} · {config.phone} ·
            <a href={`mailto:${config.email}`}>{config.email}</a>
          </div>
          <p className="lead mb-5">
            Soy Ingeniero de Telecomunicaciones con la especialidad de Telemática por la Universidade de Vigo.
            Tengo amplia experiencia en el desarrollo Fullstack de Servicios Web en diferentes tecnologías aplicando
            siempre las últimas tendencias y frameworks de desarrollo para conseguir un Software de calidad
            y con las mejores prácticas de la industria. Mis conocimientos abarcan desde el diseño y análisis funcional,
            modelado de datos, manejo de bases de datos, backend, frontend, CICD mediante Gitlab. Soy un apasionado de
            las tecnologías de virtualización como Docker y Kubernetes. 
            Y disfruto de la realización de páginas web estáticas en diferentes tecnologías como Gatsby o Hexo.
          </p>
          <div className="social-icons">
            {config.socialLinks.map(social => {
              const { icon, url } = social;
              return (
                <a key={url} href={url}>
                  <i className={`fab ${icon}`}></i>
                </a>
              );
            })}
          </div>
        </div>
      </section>
    );
  }
}

export default About;

