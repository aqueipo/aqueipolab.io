import React, { Component } from 'react';
import Experience from './Experience';

export class Experiences extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { tabs } = this.state;
    return (
	          <section
        className="resume-section p-3 p-lg-5 d-flex justify-content-center"
        id="experience"
      >
        <div className="w-100">
          <h2 className="mb-5">Experiencia</h2>

          {this.props.experiences.edges.map(exp => (
                  <Experience exp={exp}/>
          ))}
        </div>
      </section>
    );
  }
}

export default Experiences;

