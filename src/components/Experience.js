import React, { Component } from 'react';
import Moment from 'react-moment';
import 'moment/locale/es';


export class Experience extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { tabs } = this.state;
    return (
	    <div className="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
              <div className="resume-content">
                <h3 className="mb-0">{this.props.exp.node.frontmatter.job}</h3>
                <div className="subheading mb-3">{this.props.exp.node.frontmatter.company}</div>
                <div dangerouslySetInnerHTML={{ __html: this.props.exp.node.html }} />

              </div>
              <div className="resume-date text-md-right">
                <span className="text-primary">
                  <Moment locale="es" format="MMMM YYYY">{this.props.exp.node.frontmatter.start}</Moment> - {this.props.exp.node.frontmatter.end !== "" &&
                  <Moment locale="es" format="MMMM YYYY">{this.props.exp.node.frontmatter.end}</Moment>
                  }
                  {this.props.exp.node.frontmatter.end === "" &&
                  <span>Actualidad</span>
                  }
                </span>
              </div>
            </div>

    );
  }
}

export default Experience;

