import React, { Component } from 'react';

export class Certifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { tabs } = this.state;
    return (
        <section
        className="resume-section p-3 p-lg-5 d-flex align-items-center"
        id="awards"
      >
        <div className="w-100">
          <h2 className="mb-5">Cursos y Certificaciones</h2>
          <ul className="fa-ul mb-0">
            {this.props.certifications.edges.map(cert => (
              <li>
                <i className="fa-li fa fa-graduation-cap text-warning"></i>
                <a href={cert.node.frontmatter.bag_url}>{cert.node.html}</a>
              </li>
            ))}

            <li>
              <i className="fa-li fa fa-graduation-cap text-warning"></i>
              <a href="https://www.coursera.org/api/legacyCertificates.v1/spark/statementOfAccomplishment/974484~2748407/pdf">Web Application Architectures, University of Mexico (Coursera)</a>
            </li>
            <li>
              <i className="fa-li fa fa-graduation-cap text-warning"></i>
              <a href="https://www.coursera.org/api/legacyCertificates.v1/spark/statementOfAccomplishment/972612~2748407/pdf">From GPS and Google Maps to Spatial Computing, University of Minnesota (Coursera)</a>
            </li>
            <li>
              <i className="fa-li fa fa-graduation-cap text-warning"></i>1<sup>st</sup>
              <a href="https://www.coursera.org/api/legacyCertificates.v1/spark/statementOfAccomplishment/972304~2748407/pdf">Machine Learning, Stanford University (Coursera)</a>
            </li>
            <li>
              <i className="fa-li fa fa-graduation-cap text-warning"></i>
              <a href="#">Heterogeneous Parallel Programming, University of Illinois at Urbana-Champaign (Coursera)</a>
            </li>
            <li>
              <i className="fa-li fa fa-graduation-cap text-warning"></i>
              <a href="https://www.coursera.org/api/legacyCertificates.v1/spark/statementOfAccomplishment/972094~2748407/pdf">Programming Cloud Services for Android Handheld Systems, Vanderbilt University (Coursera)</a>
            </li>
            <li>
              <i className="fa-li fa fa-graduation-cap text-warning"></i>
              <a href="https://miriadax.net/files/10132/badge/ae2169fa-e8df-4ea8-a48e-f0085f16ee32.pdf">Desarrollo de servicios en la nube con HTML5,
Javascript y node.js, Universidad Politécnica de Madrid (MiriadaX)</a>
            </li>
            <li>
              <i className="fa-li fa fa-bookmark text-warning"></i>
              <a href="https://dialnet.unirioja.es/servlet/articulo?codigo=6893788">BeA add-ons to support on-line assessment and to improve review communications,  The International journal of engineering education ISSN-e 0949-149X, Vol. 33, no. Extra 2 (Parte B), 2017</a>
            </li>
            <li>
              <i className="fa-li fa fa-bookmark text-warning"></i>
              <a href="https://ieeexplore.ieee.org/abstract/document/7451642">Improving the communication with the students within the assessment stage using BeA,  2015 International Symposium on Computers in Education (SIIE), Electronic ISBN: 978-1-5090-1435-4</a>
            </li>
          </ul>
        </div>
      </section>
    );
  }
}

export default Certifications;