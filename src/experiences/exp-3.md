---
content: "experiences"
job: R&D Engineer
company: Universidade de Vigo
start: 2014-05-15
end: 2016-12-31
date: 2019-10-15
title: exp-3.md
---
<p>Desarrollo de servicios web y aplicaciones móviles para la plataforma iOS en el ámbito del e-learning.</p>
<ul>
    <li>
        <div>Aplicaciones móviles:</div>
        <ul>
            <li>Objective-C, Swift, C/C++</li>
        </ul>
    </li>
    <li>
        <div>Desarrollo Web:</div>
        <ul>
            <li>JavaEE, Struts</li>
            <li>NodeJS</li>
            <li>Git</li>
            <li>BBDD MySQL y PostgreSQL</li>
        </ul>
    </li>
</ul>

