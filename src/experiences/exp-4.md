---
content: "experiences"
path: "/src/experiences/exp-4.md"
date: "2019-05-04"
job: "Software Engineer"
company: "Ibertenis"
title: "exp-4"
start: "2016-06-01"
end: ""
---
<p>Desarrollo FullStack de servicios web en el ámbito de Federaciones deportivas, Clubs, Escuelas deportivas y e-Commerce</p>
<ul>
    <li>Odoo</li>
    <li>Backend Django Framework</li>
    <li>Frontend Javascript, jQuery, React</li>
    <li>CICD Gitlab</li>
    <li>BBDD MySQL, MariaDB</li>
    <li>Docker</li>
</ul>
