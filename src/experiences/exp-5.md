---
content: "experiences"
path: "/src/experiences/exp-5.md"
date: "2019-05-04"
job: "Software Engineer"
company: "Qindel"
title: "exp-5"
start: "2017-03-15"
end: ""
---
<p>Desarrollo de servicios web y aplicaciones móviles para Inditex</p>
<ul>
    <li>Backend con spring boot</li>
    <li>Frontend React</li>
    <li>Openshift</li>
    <li>Git</li>
    <li>BBDD Oracle DB2, MySQL, MariaDB, Postgresql</li>
</ul>
